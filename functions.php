<?php

	function CreateLog($op,$obj) {	
		$sessid=session_id();		
		$sess=SelectDB('sessions',"`session` like '$sessid'");
		$user=$sess[0]['user']; 
		$users=SelectDB("users","`id`=".$user);
		$a['dt']=Date("Y-m-d H:i:s");		
		$a['op']=(int)$op;
		$role=$users[0]['role'];
		switch ($role) {
			case 1 : case 2 : case 3 : case 6 :
				$subj=$sess[0]['operator'];
			break;
			case 5 : 
				$admins=SelectDBv("admins","`status`=".$user);
				if ($admins) $subj=$admins['id'];
			break;
		}
		$a['role']=(int)$role;
		$a['subj']=(int)$subj;
		$a['obj']=(int)$obj;
		InsertDB("logs",$a);
		
	}

	function ScanPromo($domain) {
		$os=SelectDB("orders","`typedis` IS NULL AND `domain`=".$domain,10000,"`id` DESC");
		if ($os) foreach ($os as $i =>$o) {
			$a=array();
			$td=SelectDB("items","`idorder`=$o[id]");
			if ($td) foreach ($td as $j =>$it) $a[]=(int)$it["cause"];
			if (count($a)) {
				$cause=array_sum($a)/count($a);
				switch ($cause) {
					case 0 : case 1 : case 2 : case 3 : $cause=$cause; break;
					default : $cause=-1; break;
				}
				UpdateDBi("orders","`id`=$o[id]",array("typedis"=>$cause));
			}	
		}
	}

	function phone10($s,$domain) {
		$s=substr($s,-10);
		while (strlen($s)<10) $s="0".$s;
		return $s;
	}
	
	function genaddr($idadr) {
		$da=json_decode(htmlspecialchars_decode(SelectDBf("listadr",$idadr,"adr")),true);
		$location=SelectDBf("locations",SelectDBf("listadr",$idadr,"location"),"name");
		$adr=$location.", ";
		if ($da['street']) $ada=$da['street'].($da['house']?",".$da['house']:"");
			elseif ($da['address']) $ada=$da['address'];
		$adr.=$ada.", ";		
		if ($da['apartment']) $adr.="кв.".$da['apartment'].", ";
		if ($da['entrance']) $adr.="под.".$da['entrance'].", ";
		if ($da['floor']) $adr.="эт.".$da['floor'];				
		return $adr;
	}

	function FullAddr($id) {	
		$idadr=SelectDBf("orders",$id,"idadr");
		$da=json_decode(htmlspecialchars_decode(SelectDBf("listadr",$idadr,"adr")),true);
		$location=SelectDBf("locations",SelectDBf("listadr",$idadr,"location"),"name");
		$adr=$location.", ";
		if ($da['street']) $ada=$da['street'].($da['house']?",".$da['house']:"");
			elseif ($da['address']) $ada=$da['address'];
		$adr.=$ada.", ";		
		if ($da['apartment']) $adr.="кв.".$da['apartment'].", ";
		if ($da['entrance']) $adr.="под.".$da['entrance'].", ";
		if ($da['floor']) $adr.="эт.".$da['floor'];						
		return $adr;
	}

    function timetosec($pt) {
        $at=explode(":",$pt);
		switch (count($at)) {
			case 3 : $h=(int)$at[0]; $m=(int)$at[1]; $s=(int)$at[2]; break; 
			case 2 : $s=0; $h=(int)$at[0]; $m=(int)$at[1]; break; 
			case 1 : $s=0; $m=0; $h=(int)$at[0]; break; 
			case 0 : $h=0; $m=0; $s=0; break; 
		}
        return $h*3600+$m*60+$s;
    }

    function timetosec24($pt) {
        $at=explode(":",$pt);
		switch (count($at)) {
			case 3 : $h=(int)$at[0]; $m=(int)$at[1]; $s=(int)$at[2]; break; 
			case 2 : $s=0; $h=(int)$at[0]; $m=(int)$at[1]; break; 
			case 1 : $s=0; $m=0; $h=(int)$at[0]; break; 
			case 0 : $h=0; $m=0; $s=0; break; 
		}
		if ($h<5) $h=$h+24;
        return $h*3600+$m*60+$s;
    }

    function sectotime($sec)
    {
        $h=floor($sec/3600); if ($h<10) $h="0".$h;
        $m=(floor(($sec%3600)/60)); if ($m<10) $m="0".$m;
        $s=($sec%60); if ($s<10) $s="0".$s;
        return $h.":".$m.":".$s;
    }

    function dateL($d)
    {
        $a=explode("-",$d);
		if (count($a)==1) return strtr($d,array('/'=>".")); else return $a[2].".".$a[1].".".$a[0];
    }

    function dateLs($d)
    {
        $a=explode("-",$d);
		if (count($a)==1) return strtr($d,array('/'=>".")); else return $a[2].".".$a[1];
		}

    function parseDate($d) {
		$mask=preg_replace('/[0-9]/','',$d);
		$a=explode(substr($mask,0,1),$d);
		switch ($mask) {
			case '..' : $res=$a[2]."-".$a[1]."-".$a[0]; break;
			case '//' : $res=$a[2]."-".$a[0]."-".$a[1]; break;
			case '--' : $res=$d; break;
			case '.' : $res=Date("Y")."-".$a[1]."-".$a[0]; break;
		}
		return $res;
	}

	function parseModule($name,$vars) {
		global $tags;	
		$handlerfun="handler_module_".strtr($name,array("."=>"_"));
		if (file_exists(ROOT."/modules/".$name.".html")) { $canvas=file_get_contents(ROOT."/modules/".$name.".html"); } else return "";
		$canvas = preg_replace_callback('|{(.+)}|isU', function($matches) use ($vars) {
			global $tags;		
			static $id = 0;
			$id++;
			$var=substr($matches[0],2,-1);
			if ($matches[0][1]=="#") return $tags[$var];
				elseif ($matches[0][1]=="$") return $vars[$var];
					elseif ($matches[0][1]=="%") return $that->parseModule($var);
						else return $matches[0];
		},$canvas,-1);
		return $canvas;
	}

	function ListDB($table,$field,$req,$select) {
		global $DB;
		$list="";
		switch ($table) {
			case "carriers" :
				$sql="SELECT `staff`.* FROM `staff`,`linkstaff` WHERE `staff`.`type`=6 AND `linkstaff`.`unit`=$req AND `linkstaff`.`staff`=`staff`.`id` ORDER BY `staff`.`id` ASC";
				$list="<option value='NULL'></option>";
			break;
			default :
				$sql="SELECT * FROM `$table` WHERE $req ORDER BY `id` ASC";
			break;
		}			
		$res = $DB->query($sql);
		if (!$res) return "";
    	while ($rw=$res->fetch_assoc()) {
			if ($rw['id']==$select) $add="selected"; else $add="";
			if (!$rw['disable']) $list.="<option $add value='$rw[id]'>".$rw[$field]."</option>";
		}
		return $list;
	}

	function InsertDB($table,$record) {
	
		global $DB;

		$tnow=Date("Y-m-d H:i:s");

		$fields=array();
		$values=array();
		
		switch ($table) {
		
			case "admins" : $record["online"]=$tnow; break;
			case "items" : case "messages" : $record["ts"]=$tnow; break;
			case "sessions" : case "clients" : case "users" : $record["updated"]=$tnow; break;
			
			case "orders" :
			break;
			
			case "cashflow" :
				if (!$record["dt"]) $record["dt"]=$tnow;
				if (!$record["km"]) $record["km"]="NULL";
			break;

			case "transfers" :
				if ($record["type"]==3) {					
					$ri=array(
						"unit"=>$record["ufrom"],
						"dt"=>$record["dt"],
						"admin"=>$record["admin"],
						"summ"=>$record["summ"],
						"type"=>1,
						"comment"=>$record["comment"]);
					$idcf=InsertDB("cashflow",$ri);
					$record["comment"].=" #".$idcf;
				}
			break;			

			case "listts" :
				UpdateDBi("listts","`idorder`=$record[idorder] AND `type`=$record[type]",array("type"=>-1*$record['type'],"ts"=>"`ts`"));
			break;

		}
		
		foreach ($record as $i =>$n) {
			$fields[]="`$i`";
			$n=strtr($n,array("'"=>"`"));
			if (($n!="CURRENT_TIMESTAMP()")&&($n!="NULL")) $values[]="'$n'"; else $values[]=$n;
		}
				
		$sql="INSERT INTO `$table` (".implode(",",$fields).") VALUES (".implode(",",$values).");";
		
		file_put_contents($_SERVER['DOCUMENT_ROOT']."/logs/inputlog".Date("dm").".asp",$sql."\n",FILE_APPEND);
		
		$res = $DB->query($sql);
		$id=$DB->insert_id;
		if (!$res) {
			file_put_contents($_SERVER['DOCUMENT_ROOT']."/logs/errolog".Date("dm").".asp",Date("Y-m-d H:m:s")." ".$sql."\n".$DB->error." ".$_SERVER['REQUEST_URI']."\n",FILE_APPEND);
			return 0;
		}

		switch ($table) {
			case "listts" :
				if ($record["type"]>13) {
					$t13=SelectDBfq("listts","`idorder`=$record[idorder] AND `type`=13","ts");
					if (!t13) {
						$record13=$record;
						$record13["type"]=13;
						foreach ($record13 as $i =>$n) {
							$fields[]="`$i`";
							$n=strtr($n,array("'"=>"`"));
							if (($n!="CURRENT_TIMESTAMP()")&&($n!="NULL")) $values[]="'$n'"; else $values[]=$n;
						}
						$sql="INSERT INTO `$table` (".implode(",",$fields).") VALUES (".implode(",",$values).");";
						$res = $DB->query($sql);
					}
				}
			break;
		}

		return $id;
	}

	function UpdateDB($table,$req,$record,$staff=0) {
	
		global $DB;

		$tnow=Date("Y-m-d H:i:s");

		switch ($table) {
			
			case "admins" : $record["online"]=$tnow; break;
			 
			case "messages" : $record["ts"]=$tnow; break;
			
			case "sessions" : case "users" : $record["updated"]=$tnow; break;
						
			case "cashflow" :
				if (!$record["dt"]) $record["dt"]=$tnow;
				if (!$record["km"]) $record["km"]="NULL";
			break;
			
			case "clients" : 			
				$record["updated"]=$tnow;
				if (!filter_var($record['email'],FILTER_VALIDATE_EMAIL)) unset($record['email']);
				if ($record['email']=="") unset($record['email']);
				if ($record['email']=="NULL") unset($record['email']);				
				file_put_contents($_SERVER['DOCUMENT_ROOT']."/logs/zzzlog.asp",json_encode($record)."\n",FILE_APPEND);
			break;

			case "orders" :
			
				$o=SelectDBv("orders",$req);
				$ts=SelectDBfq("listts","`type`=25 AND `idorder`=$o[id]","ts");
				
				if (($record["status"])&&($o["status"]!=$record["status"])) {
					$status=$record["status"];
					InsertDB("listts",array("idorder"=>$o['id'],"type"=>$status,"staff"=>$staff,"ts"=>Date("Y-m-d H:i:s")));
					if (($status>13)&&($status<18))	{
						if ($o['party']) UpdateDBi("party","`id`=$o[party]",array("status"=>1));
							else return "0";
					}
					if (($status>1)&&$staff&&(!SelectDBfq("listts","`idorder`=$o[id] AND `type`=1 AND `staff`>0","ts")))
						InsertDB("listts",array("idorder"=>$o['id'],"type"=>1,"staff"=>$staff,"ts"=>Date("Y-m-d H:i:s")));
				}
								
				if (($record["pm"]==0)&&(($o["sp"]==1)||($o["sp"]==3))) UpdateDBi("orders","`id`=$o[id]",array("sp"=>0));
				if (($record["pm"]==3)&&($o["sp"]==3)) UpdateDBi("orders","`id`=$o[id]",array("sp"=>1));
				
				if (!ts&&($record["sp"]==2)&&($o["sp"]!=$record["sp"])) InsertDB("listts",array("idorder"=>$o['id'],"type"=>25,"staff"=>$staff,"ts"=>Date("Y-m-d H:i:s")));
					elseif (!ts&&$record["status"]==18) InsertDB("listts",array("idorder"=>$o['id'],"type"=>25,"staff"=>$staff,"ts"=>Date("Y-m-d H:i:s")));
				
			break;

		}

		$sets=array();
		if ($record) foreach ($record as $i =>$n) {
			$n=strtr($n,array("'"=>"`"));
			if (($n=='NULL')||($n=='current_timestamp()')||(substr($n,0,1)=="`")) $sets[]="`$i`=$n"; else $sets[]="`$i`='$n'";
		} else return 0;
		
		$sql="UPDATE `$table` SET ".implode(",",$sets)." WHERE $req;";
		$res = $DB->query($sql);
						
		if (!$res) {
			file_put_contents($_SERVER['DOCUMENT_ROOT']."/logs/errolog".Date("dm").".asp",Date("Y-m-d H:m:s")." ".$sql."\n".$DB->error." ".$_SERVER['REQUEST_URI']."\n",FILE_APPEND);
			return 0;
		} else return 1;
		
	}

	function UpdateDBi($table,$req,$record) {
		global $DB;
		$sets=array();
		if ($record) foreach ($record as $i =>$n) {
			$n=strtr($n,array("'"=>"`"));
			if (($n=='NULL')||($n=='current_timestamp()')||(substr($n,0,1)=="`")) $sets[]="`$i`=$n"; else $sets[]="`$i`='$n'";
		} else return 0;		
		$sql="UPDATE `$table` SET ".implode(",",$sets)." WHERE $req;";
		$res = $DB->query($sql);
		if (!$res) {
			file_put_contents($_SERVER['DOCUMENT_ROOT']."/logs/errolog".Date("dm").".asp",Date("Y-m-d H:m:s")." ".$sql."\n".$DB->error." ".$_SERVER['REQUEST_URI']."\n",FILE_APPEND);
			return 0;
		} else return 1;
	}

	function UpdateConfig($id,$record) {
		global $DB;
		foreach ($record as $i =>$n) {
			$sql="DELETE FROM `config` WHERE `user`=$id AND `name` like '$i'";
			$res = $DB->query($sql);
			$sql="INSERT INTO `config` (`user`,`name`,`value`) VALUES ($id,'$i','$n');";
			$res = $DB->query($sql);
		}
	}

	function LoadConfig($id) {
		global $DB;
		$list=array();
		$sql="SELECT * FROM `config` WHERE `user`=$id";
		$res = $DB->query($sql);		
		if (!$res) {
			file_put_contents($_SERVER['DOCUMENT_ROOT']."/logs/errolog".Date("dm").".asp",Date("Y-m-d H:m:s")." ".$sql."\n".$DB->error." ".$_SERVER['REQUEST_URI']."\n",FILE_APPEND);
			return 0;
		}	
    	while ($rw=$res->fetch_assoc()) $list[$rw['name']]=$rw['value'];
		return $list;
	}
	
	function ClearDB($table) {
		global $DB;
		$sql="TRUNCATE `$table`";
		$res = $DB->query($sql);
	}
	
	function SelectDB($table,$req=1,$limit=0,$order="`id` ASC",$byid=false) {
		global $DB;
		$list=array();
		$sql="SELECT * FROM `$table` WHERE $req order by $order ".($limit?"LIMIT $limit":"");
		$res = $DB->query($sql);
		if (!$res) {
			file_put_contents($_SERVER['DOCUMENT_ROOT']."/logs/errolog".Date("dm").".asp",Date("Y-m-d H:m:s")." ".$sql."\n".$DB->error." ".$_SERVER['REQUEST_URI']."\n",FILE_APPEND);
			return 0;
		}	
    	while ($rw=$res->fetch_assoc()) {
			if ($byid) $list[$rw['id']]=$rw; else $list[]=$rw;
		}
		return $list;
	}

	function SelectDBc($table,$req=1,$field="name",$order="`id` ASC") {
		global $DB;
		$list=array();
		$sql="SELECT * FROM `$table` WHERE $req ORDER BY $order";
		$res = $DB->query($sql);
		if (!$res) {
			file_put_contents($_SERVER['DOCUMENT_ROOT']."/logs/errolog".Date("dm").".asp",Date("Y-m-d H:m:s")." ".$sql."\n".$DB->error." ".$_SERVER['REQUEST_URI']."\n",FILE_APPEND);
			return 0;
		}	
    	while ($rw=$res->fetch_assoc()) {
			$list[$rw['id']]=$rw[$field];
		}
		return $list;
	}

	function SelectSQL($sql) {
		global $DB;
		$list=array();
		$res = $DB->query($sql);
		if (!$res) {
			file_put_contents($_SERVER['DOCUMENT_ROOT']."/logs/errolog".Date("dm").".asp",Date("Y-m-d H:m:s")." ".$sql."\n".$DB->error." ".$_SERVER['REQUEST_URI']."\n",FILE_APPEND);
			return 0;
		}	
    	while ($rw=$res->fetch_assoc()) $list[]=$rw;
		return $list;
	}

	function SelectDBx($list,$tables,$req,$limit,$order) {
		global $DB;		
		$sql="SELECT $list FROM $tables WHERE $req ORDER BY $order ".($limit?"LIMIT $limit":"");
		$res = $DB->query($sql);				
		if (!$res) {
			file_put_contents($_SERVER['DOCUMENT_ROOT']."/logs/errolog".Date("dm").".asp",Date("Y-m-d H:m:s")." ".$sql."\n".$DB->error." ".$_SERVER['REQUEST_URI']."\n",FILE_APPEND);
			return 0;
		} /// else file_put_contents($_SERVER['DOCUMENT_ROOT']."/logs/xlog.asp",$sql."\n",FILE_APPEND);
    	while ($rw=$res->fetch_assoc()) $rez[]=$rw;
		return $rez;
	}

	function SelectDBf($table,$id,$field) {
		global $DB;
		$sql="SELECT `$field` FROM `$table` WHERE `id`=$id";
		$res = $DB->query($sql);
		if (!$res) {
			file_put_contents($_SERVER['DOCUMENT_ROOT']."/logs/errolog".Date("dm").".asp",Date("Y-m-d H:m:s")." ".$sql."\n".$DB->error." ".$_SERVER['REQUEST_URI']."\n",FILE_APPEND);
			return 0;
		}	
    	if ($rw=$res->fetch_assoc()) return $rw[$field];
			else return 0;
	}

	function SelectDBfq($table,$req,$field) {
		global $DB;
		$sql="SELECT `$field` FROM `$table` WHERE $req";
		$res = $DB->query($sql);
		if (!$res) {
			file_put_contents($_SERVER['DOCUMENT_ROOT']."/logs/errolog".Date("dm").".asp",Date("Y-m-d H:m:s")." ".$sql."\n".$DB->error." ".$_SERVER['REQUEST_URI']."\n",FILE_APPEND);
			return 0;
		}	
    	if ($rw=$res->fetch_assoc()) return $rw[$field];
			else return 0;
	}

	function SelectDBv($table,$req) {
		global $DB;
		$sql="SELECT * FROM `$table` WHERE $req";
		$res = $DB->query($sql);
		if (!$res) {
			file_put_contents($_SERVER['DOCUMENT_ROOT']."/logs/errolog".Date("dm").".asp",Date("Y-m-d H:m:s")." ".$sql."\n".$DB->error." ".$_SERVER['REQUEST_URI']."\n",FILE_APPEND);
			return 0;
		}	
    	if ($rw=$res->fetch_assoc()) return $rw; else return 0;
	}

	function TableDB($table) {
		global $DB;
		$sql="SHOW FULL COLUMNS FROM `$table`";
		$query = $DB->query($sql);
		while($row = $query->fetch_assoc()) $res[] = $row;
		return $res;
	}

	function countDBreq($table,$req=1) {
		global $DB;
		$sql="SELECT count(id) FROM `$table` WHERE $req";
		$res = $DB->query($sql);		
		if (!$res) {
			file_put_contents($_SERVER['DOCUMENT_ROOT']."/logs/errolog".Date("dm").".asp",Date("Y-m-d H:m:s")." ".$sql."\n".$DB->error." ".$_SERVER['REQUEST_URI']."\n",FILE_APPEND);
			return 0;
		}	
    	if ($rw=$res->fetch_assoc()) return $rw['count(id)'];
		return 0;
	}

	function DeleteDB($table,$req) {
		global $DB;		
		$idai=false;		
		file_put_contents($_SERVER['DOCUMENT_ROOT']."/logs/dellog".Date("dm").".asp",$table." ".$req."\n",FILE_APPEND);
		$sql="DELETE FROM `$table` WHERE $req";
		$res = $DB->query($sql);
		if (!$res) {
			file_put_contents($_SERVER['DOCUMENT_ROOT']."/logs/errolog".Date("dm").".asp",Date("Y-m-d H:m:s")." ".$sql."\n".$DB->error." ".$_SERVER['REQUEST_URI']."\n",FILE_APPEND);
			return 0;
		}
		/*
		$sql="SHOW COLUMNS FROM `errors`";
		$rz = $DB->query($sql);
		while ($col=$rz->fetch_assoc()) 
			if (($col['Field']=="id")&&($col['Key']=="PRI")&&($col['Extra']=="auto_increment")) $idai=true;
		if ($idai) {
			$max=MaxDB($table,'id');
			$sql="ALTER TABLE `$table` AUTO_INCREMENT=".($max+1);
			$rz = $DB->query($sql);
		}*/		
		return 1;
	}
	
	function loadform($a,$u) {
	
		$id=(int)$a['id'];
		$form=$a['form'];
		
		switch ($form) {
		
			case 'tags' :
				$i=0;
				if ($id==0) $att=SelectDB("usertags","`user`=".$u,0,"`tag` asc");
					else $att=SelectDB("tracktags","`track`=$id",0,"`tag` asc");
				if ($att) foreach ($att as $i1=>$n) $tt[$n['tag']]=1;
				$tgg=SelectDB("tags","`parent`=0 and `id`>0",0,"`pos` asc");
				foreach($tgg as $i1 =>$tg) {
					$list="";
					$i++;
					$tgs=SelectDB("tags","`parent`=$tg[id] and `id`>0",0,"`pos` asc");
					foreach($tgs as $i2 =>$t) {
						if ($tt[$t['id']]) $on=" on"; else $on="";
						$list.="<li><div class='tag li$on' tag='$t[id]'>$t[name]</div></li>";
					}
					if ($list!='') $list="<ul>$list</ul>";					  
					if ($tt[$tg['id']]) $on=" on"; else $on="";
					$l.="<li><div class='tag ul$on' tag='$tg[id]'>$tg[name]</div>$list</li>";
					if (in_array($i,array(4,8,12,14,21))) { $r.="<ul class='tabletags'>$l</ul>"; $l=""; }
				}				
				if ($i!=21) $r.="<ul class='tabletags'>$l</ul>";
				echo $r;	
			break;
		}
		
	}
	
	function EqTags($id,$u) {
	
		global $DB;
		$tgs=SelectDB("tags");
		if ($tgs) foreach ($tgs as $i1=>$n) $parents[$n['tag']]=$n['parent'];		
		$ut=array();
		$tt=array();
		$aut=SelectDB("usertags","`user`=".$u,0,"`tag` asc");
		if ($aut) foreach ($aut as $i1=>$n) $tframe[$parents[$n['tag']]][$n['tag']]=1;
		$att=SelectDB("tracktags","`track`=$id",0,"`tag` asc");
		if ($att) foreach ($att as $i1=>$n) $ttrack[$parents[$n['tag']]][$n['tag']]=1;
		if (!is_array($tframe)) return 1;
		foreach ($tframe as $i => $af)
		{
			$ex=true;
			if (!is_array($ttrack[$i])) return 0;
			if (count($ttrack[$i])==0) return 0;
			foreach ($ttrack[$i] as $j => $n)
				if (isset($af[$j])) $ex=false;		
				if ($ex)
				foreach ($ttrack[$i] as $j => $n)
					if (!isset($af[$j])) return 0;
		}
		return 1;
	}

	function MaxDB($table,$field) {
		global $DB;
		$list=array();		
		$sql="SELECT $field FROM `$table` WHERE 1 order by `$field` desc";
		$res = $DB->query($sql);
		if (!$res) {
			file_put_contents($_SERVER['DOCUMENT_ROOT']."/logs/errolog".Date("dm").".asp",Date("Y-m-d H:m:s")." ".$sql."\n".$DB->error." ".$_SERVER['REQUEST_URI']."\n",FILE_APPEND);
			return 0;
		}	
    	if ($rw=$res->fetch_assoc()) return $rw[$field];
		return 0;
	}
	
	function SortPos($table) {
		$pos=1;
		$lst=SelectDB($table,1,0,"`pos` asc");
		if ($lst) foreach($lst as $i =>$t) UpdateDB($table,"`ts`=".$t['ts'],array("pos"=>$pos++));
	}

	function countDB($table,$field) {
		global $DB;
		$sql="SELECT count($field) FROM `$table` WHERE 1";
		$res = $DB->query($sql);				
		if (!$res) $c=0; elseif ($rw=$res->fetch_assoc()) $c=$rw["count($field)"];
		$sql="SELECT count(DISTINCT $field) as count FROM `$table` WHERE 1";
		$res = $DB->query($sql);				
		if (!$res) $u=0; elseif ($rw=$res->fetch_assoc()) $u=$rw["count"];
		return array($c,$u);
	}
	
	function countPacket($id) {
		global $DB;
		$table="packet".$id;
		list($r['count'],$r['ucount'])=countDB($table,"track");
		$r['duration']=0;
		$rs=SelectDB($table,1,0,"`pos` asc");
		if ($rs) foreach($rs as $i =>$t) {
			$atr=SelectDB("tracks","`id`=".$t['track']);
			if ($atr) $tr=$atr[0];
			$r['duration']+=timetosec($tr['playtime']);
		}
		$r['dupdate']=Date("Y-m-d H:i:s");
		UpdateDB('packets',"`id`=".$id,$r);
	}
	
	function shiftA($a,$d) {
		if ((count($a)==0)||($d==0)) return $a;
		$res=array();
		$i=$d;
		while (count($res)<count($a)) {
			$res[]=$a[$i];
			$i++;
			if ($i>=count($a)) $i=0;
		}
		return $res;
	}
	
	function LoadTags($LANG) {
		global $tags;
		$at=SelectDB("content");
		foreach ($at as $i =>$n) $tags[$n['id']]=$n[$LANG];
	}

	function convertdate($s) {
		$p=explode(".",$s);
		if (count($p)==1) $r=date("Y-m-d"); 
			elseif (count($p)==2) $r=date("Y")."-".$p[1]."-".$p[0];
				else $r=$p[2]."-".$p[1]."-".$p[0];
		return $r;
	}

	function sendemail($domain,$type,$id,$lg,$email) {
	
		global $DB,$tags;
		
		LoadTags($lg);
				
		switch ($domain) {
		
			case 1 :
				$dname="ninjasushi.az";		
				$curr="AZN";
				$city="Баку";				
			break;
			
			case 2 :
				$dname="ninjasushi.com.ua";		
				$curr="грн.";
				$city="Киев";
			break;

			case 3 :
				$dname="ninjasushi.moscow";		
				$curr="руб.";
				$city="Москва";
			break;

		}
		
		$headers .= "Reply-To: info <info@{$dname}>\r\n"; 
		$headers .= "Return-Path: info <info@{$dname}>\r\n";
		$headers .= "From: info <info@{$dname}>\r\n"; 
		$headers .= "Organization: {$dname}\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=utf-8\r\n";
		$headers .= "X-Priority: 3\r\n";		
		$headers .= "X-Mailer: PHP". phpversion() ."\r\n";
		
		switch ($type) {
		
			case 1 : // reg
				$title=$tags[235];
				$lp['h1']=$tags[236];
				$lp['body']=$tags[237];
				$lp['domain']=$dname;
				$message=parseModule('email',$lp);
			break;
			
			case 2 : // order in
			
				$o=SelectDBv("orders","`id`=".$id);
				
				$u=SelectDBv("clients","`id`=".$o['client']);
				$client="";
				if ($u["login"]) $client=$u["login"];
				if ($u["email"]) $client.=" ( ".$u["email"]." ) ";
				if ($client=="") $client=$tags[262];
								
				$title=$tags[238]." ".$id;
				
				$data=SmartUnS($o['data']);
								
				if ($data['payment-method']=="card") $metp=$tags[99]; else $metp=$tags[98];				 
				
				$table="<table class='items'><tr><th>".$tags[250]."</th><th>".$tags[223]."</th><th>".$tags[83]."</th></tr>";
				$ol=SelectDB("items","`idorder`=".$id);
				if ($ol) foreach ($ol as $i=>$o) {
					$it=SelectDBv("catalog","`id`=".$o["id"]);
					$table.="<tr><td>".$it[$lg]."</td><td>".$o['qty']."</td><td>".$o['price']." $curr</td></tr>";
					$sum+=$o['qty']*$o['price']-$o['bonus'];
				}
				$table.="<tr><td colspanf>".$tags[173]."</td><td>".$metp."</td></tr>";
				$table.="<tr><td colspan=2>".$tags[251]."</td><td>".$sum." $curr</td></tr>";
				$table.="</table>";

				$name=$data['cartName'];
				$phone=$data['cartTel'];
				
				$adr=$tags[161]." ".$data['adr1'].", ";
				$adr.=$tags[162]." ".$data['adr2'].", ";
				$adr.=$tags[163]." ".$data['adr3'].", ";
				$adr.=$tags[164]." ".$data['adr4'].", ";
				$adr.=$tags[165]." ".$data['adr5'].", ";
				$adr.=$tags[166]." ".$data['adr6'];
				
				if (!$data['day']) $data['day']=$tags[169];
				if (!$data['time']) $data['time']=$tags[171];
				
				$time=$data['day'].",".$data['time'];
				$persons=$data['people'];
				if (strlen($persons)<3) $persons.=" ".$tags[178];
				
				if ($data['cat']) $addon.=$tags[180].",";
				if ($data['dog']) $addon.=$tags[181].",";
				if ($data['change']) $addon.=$tags[175]." ".$data['change']." ".$curr;
				
				$comm=$data['comment'];

				$from=$tags[239];
				eval("\$from=\"$from\";");
				
				$lp['h1']=$tags[238];
				$body="<h4 style='text-align:center'>".$from." ".$tags[240].":</h4>";
				$body.="<h2 style='text-align:center;color:red'>".$tags[245]." № ".$id."</h2>";
				$body.="<h4>".$tags[261]." : ".$client."</h4>";
				$body.=$table;
				$body.="<h3 style='text-align:center'>".$tags[249]."</h3>";
				$body.="<table>";
				$body.="<tr><td>".$tags[160]."</td><td>".$tags[190]."</td></tr>";
				$body.="<tr><td><b>".$city."</b></td><td><b>".$phone."</b></td></tr>";
				$body.="<tr><td>".$tags[202]."</td><td>".$tags[177]."</td></tr>";
				$body.="<tr><td><b>".$adr."</b></td><td><b>".$persons."</b></td></tr>";
				$body.="<tr><td>".$tags[247]."</td><td>".$tags[248]."</td></tr>";
				$body.="<tr><td><b>".$time."</b></td><td><b>".$addon."</b></td></tr>";
				$body.="<tr><td colspan=2><b>".$comm."</td></tr>";

				$lp['body']=$body;
				$lp['domain']=$dname;
				$message=parseModule('email',$lp);
				
			break;
			
			case 3 : // order out
			
				$o=SelectDBv("orders","`id`=".$id);
				$u=SelectDBv("clients","`id`=".$o['client']);
								
				$data=SmartUnS($o['data']);

				if ($data['payment-method']=="card") $metp=$tags[99]; else $metp=$tags[98];
				
				$table="<table class='items'><tr><th>".$tags[250]."</th><th>".$tags[223]."</th><th>".$tags[83]."</th></tr>";
				$ol=SelectDB("items","`idorder`=".$id);
				if ($ol) foreach ($ol as $i=>$o) {
					$it=SelectDBv("catalog","`id`=".$o["id"]);
					$table.="<tr><td>".$it[$lg]."</td><td>".$o['qty']."</td><td>".$o['price']." $curr</td></tr>";
					$sum+=$o['qty']*$o['price'];
				}
				$table.="<tr><td colspan=2>".$tags[173]."</td><td>".$metp."</td></tr>";
				$table.="<tr><td colspan=2>".$tags[251]."</td><td>".$sum." $curr</td></tr>";
				$table.="</table>";

				$name=$data['cartName'];
				$phone=$data['cartTel'];
								
				$adr=$tags[161]." ".$data['adr1'].", ";
				$adr.=$tags[162]." ".$data['adr2'].", ";
				$adr.=$tags[163]." ".$data['adr3'].", ";
				$adr.=$tags[164]." ".$data['adr4'].", ";
				$adr.=$tags[165]." ".$data['adr5'].", ";
				$adr.=$tags[166]." ".$data['adr6'];

				if (!$data['day']) $data['day']=$tags[169];
				if (!$data['time']) $data['time']=$tags[171];
				
				$time=$data['day'].",".$data['time'];
				$persons=$data['people'];
				if (strlen($persons)<3) $persons.=" ".$tags[178];
				
				if ($data['cat']) $addon.=$tags[180].",";
				if ($data['dog']) $addon.=$tags[181].",";
				if ($data['change']) $addon.=$tags[175]." ".$data['change']." ".$curr;
				
				$comm=$data['comment'];

				$from=$tags[239];
				eval("\$from=\"$from\";");
				
				$lp['h1']=$tags[245]." № ".$id;
				$title=$tags[245]." Ninja Sushi ".$id;

				$body.="<h3 style='text-align:center'>".$tags[249]."</h3>";
				$body.=$table;
				$body.="<table>";
				$body.="<tr><td>".$tags[160]."</td><td>".$tags[190]."</td></tr>";
				$body.="<tr><td><b>".$city."</b></td><td><b>".$phone."</b></td></tr>";
				$body.="<tr><td>".$tags[202]."</td><td>".$tags[177]."</td></tr>";
				$body.="<tr><td><b>".$adr."</b></td><td><b>".$persons."</b></td></tr>";
				$body.="<tr><td>".$tags[247]."</td><td>".$tags[248]."</td></tr>";
				$body.="<tr><td><b>".$time."</b></td><td><b>".$addon."</b></td></tr>";
				$body.="<tr><td colspan=2><b>".$comm."</td></tr>";

				$lp['body']=$body;
				$lp['domain']=$dname;
				
				$message=parseModule('email',$lp);
				
			break;			

			case 4 : // recov
			
				$title=$tags[142]." Ninja Sushi";
				$h1=$tags[142];
				$code=md5($email.date("Y~m~d"));
				$body=$tags[271]." - <h3>".$code.'</h3><br>'.$tags[252].'<br>'.$tags[253].'<a href="https://'.$dname.'/account/edit-account/?recov='.$code.'"><h3>'.$tags[254].'</h3></a>';
				$lp['h1']=$h1;
				$lp['body']=$body;
				$lp['domain']=$dname;
				$message=parseModule('email',$lp);
				
			break;

			case 5 : // rereg
			
				$title=$tags[142]." Ninja Sushi";
				$h1=$tags[142];
				$code=md5($email.date("Y~m~d"));
				$body=$tags[270]."<>".$tags[271]." - <h3>".$code.'</h3><br>'.$tags[252].'<br>'.$tags[253].'<a href="https://'.$dname.'/account/edit-account/?recov='.$code.'"><h3>'.$tags[254].'</h3></a>';
				$lp['h1']=$h1;
				$lp['body']=$body;
				$lp['domain']=$dname;
				$message=parseModule('email',$lp);
				
			break;

		}

		file_put_contents($_SERVER['DOCUMENT_ROOT']."/logs/emaillog".Date("dm").".asp",$email." ".serialize($lp)."\n",FILE_APPEND);

		mail($email, $title , $message , $headers);

	}

	function editorder($idorder) {
		
		global $page;
		
		$oi=SelectDB('items',"`idorder`=".$idorder,0,"`ts` ASC");
		
		$liststa="";
		$asta=SelectDB("tdis","`id`<4",0,"`id` ASC",true);
		foreach($asta as $i=>$n) {
			$liststa.='<span class="dropdown-item setcause" status="'.$n['id'].'" style="padding:2px 10px"><i class="fa '.$n['ico'].'"></i> '.$n['name'].'</span>';
			$liststaa.='<span class="dropdown-item setallcause" status="'.$n['id'].'" style="padding:2px 10px"><i class="fa '.$n['ico'].'"></i> '.$n['name'].'</span>';
		}	

		$mech="";
		$total=0;
		$totalbonus=0;
		
		foreach($oi as $j=>$it) {
		
			$name=selectDBf("catalog",$it['id'],"ru");
			$wei=(int)selectDBf("catalog",$it['id'],"weight");
			$price=$it['price'];
			$bonus=$it['bonus'];
			$colsum=$it['qty']*$it['price'];
			$total+=$colsum;
			$totalbonus+=$bonus;
			
			$val=$asta[$it['cause']]['ico'];

			$typebonus='
		<div class="btn-group">
			<button type="button" class="btn btn-primary" style="padding:2px 10px"><i class="fa '.$val.'"></i></button>
			<button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="padding:2px 10px"><span class="sr-only">v</span></button>
			<div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(64px, 35px, 0px); top: 0px; left: 0px; will-change: transform;" id="'.$idorder.".".$it['id'].'">'.$liststa.'
			</div>
		</div>';

			$mech.='<tr><th scope="row">'.($j+1).'</th><td>'.$name.'</td><td>'.$wei.'</td><td>'.$price.'</td><td>'.$colsum.'</td><td contenteditable=true>'.$bonus.'</td><td>'.$typebonus.'</td><td contenteditable=true>'.$it['qty'].'</td><td><i class="fa fa-2x fa-check-circle changeitem" item="'.$it['id'].'" id="'.$idorder.".".$it['id'].'"></i>&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-2x fa-times-circle deleteitem" id="'.$idorder.".".$it['id'].'"></i></td></tr>';
		}
			
		$mech.='
		<tr><th scope="row"></th><td id="tags" contenteditable=true></td><td></td><td></td><td></td><td></td><td></b></td><td id="qty" contenteditable=true>1</td><td><i class="fa fa-2x fa-plus-circle additem" id="'.$idorder.'"></i>&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-2x fa-refresh reloadpage" id="'.$idorder.".".$it['id'].'"></i></td></tr>
		<tr><th scope="row"></th><td></td><td></td><td></td><td>'.$total.'</td><td class="center">'.$totalbonus.'</td><td class="center"><b>'.($total-$totalbonus).'</b></td><td></td><td></td></tr>';

		$allbonus='
		<div class="btn-group">
			<button type="button" class="btn btn-primary" style="padding:2px 10px"><i class="fa fa-gift"></i></button>
			<button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="padding:2px 10px"><span class="sr-only">v</span></button>
			<div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(64px, 35px, 0px); top: 0px; left: 0px; will-change: transform;" id="'.$idorder.'">'.$liststaa.'
			</div>
		</div>';

		UpdateDB("orders","`id`=$idorder",array("summ"=>$total,"bonus"=>$totalbonus));
		
		return '
			<div class="bgc-white bd bdrs-3 p-30 mT-20 mB-20">
				<table class="table table-striped">
					<thead>
						<tr><th scope="col">#</th><th scope="col" width=400>Название</th><th scope="col">Вес</th><th scope="col">Стоимость</th><th scope="col">Сумма</th><th scope="col">Скидка</th><th scope="col">'.$allbonus.'</th><th scope="col">Количество</th><th scope="col"></th></tr>
					</thead>
					<tbody>
						'.$mech.'						
					</tbody>
				</table>
			</div>';
	}

	function editact($idact) {
		
		global $page;
		
		$oi=SelectDB('alines',"`idact`=$idact",0,"`ts` ASC");
		
		$mech="";
		$qty=0;
		
		if ($oi) foreach($oi as $j=>$it) {
		
			$name=selectDBf("catalog",$it['id'],"ru");
			$qty+=$it['qty'];

			$mech.='<tr><th scope="row">'.($j+1).'</th><td>'.$name.'</td><td contenteditable=true>'.$it['qty'].'</td><td><i class="fa fa-2x fa-check-circle changealine" item="'.$it['id'].'" id="'.$idact.".".$it['id'].'"></i>&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-2x fa-times-circle deletealine" id="'.$idact.".".$it['id'].'"></i></td></tr>';
		}
			
		$mech.='
		<tr><th scope="row"></th><td id="tags" contenteditable=true></td><td id="qty" contenteditable=true>1</td><td><i class="fa fa-2x fa-plus-circle addaline" id="'.$idact.'"></i></td></tr>
		<tr></tr>';

		$mech.='<tr><th scope="row"></th><td>Итого</td><td</td><td><center><b>'.$qty.'</b></center></td><td></td></tr>';

		return '
			<div class="bgc-white bd bdrs-3 p-30 mT-20 mB-20">
				<table class="table table-striped">
					<thead>
						<tr><th scope="col" width=100>#</th><th scope="col">Название</th><th scope="col" width=100>Количество</th><th scope="col" width=100></th></tr>
					</thead>
					<tbody>
						'.$mech.'						
					</tbody>
				</table>
			</div>';
	}

	function editinvoice($idinvoice,$permission=true) {
		
		global $page;
		
		$oi=SelectDB('ilines',"`idinvoice`=$idinvoice",0,"`ts` ASC");
		
		$mech="";
		$qty=0;
		$summ=0;
		
		if ($oi) foreach($oi as $j=>$it) {
			$name=selectDBf("goods",$it['id'],"name");
			$un='('.selectDBf("goods",$it['id'],"unit").')';

			$qty+=$it['qty'];
			$summ+=$it['qty']*$it['price'];
			if ($permission) $mech.='<tr><td scope="row">'.($j+1).'</td><td>'.$name.' '.$un.'</td><td contenteditable=true>'.$it['qty'].'</td><td contenteditable=true>'.$it['price'].'</td><td>'.($it['qty']*$it['price']).'</td><td><i class="fa fa-2x fa-check-circle changeiline" item="'.$it['id'].'" id="'.$idinvoice.".".$it['id'].'"></i>&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-2x fa-times-circle deleteiline" id="'.$idinvoice.".".$it['id'].'"></i></td></tr>';
				else $mech.='<tr><td scope="row">'.($j+1).'</td><td>'.$name.' '.$un.'</td><td>'.$it['qty'].'</td><td>'.$it['price'].'</td><td>'.($it['qty']*$it['price']).'</td><td></td></tr>';
		}
		
		if ($permission) $mech.='
		<tr><td scope="row"></td><td id="tags" contenteditable=true></td><td id="qty" contenteditable=true>1</td><td id="price" contenteditable=true></td><td></td><td><i class="fa fa-2x fa-plus-circle addiline" id="'.$idinvoice.'"></i></td></tr>
';

		$foot='<tr><th scope="row"></th><th>Итого</th><td</th><th><center><b>'.$qty.'</b></center></td><th></th><th><center><b>'.$summ.'</b></center></th><th></th></tr>';

		return '
			<div class="bgc-white bd bdrs-3 p-30 mT-20 mB-20">
				<table class="table table-striped">
					<thead>
						<tr><th scope="col" width=100>#</th><th scope="col">Название</th><th scope="col" width=100>Количество</th><th scope="col" width=100>Цена</th><th scope="col" width=100>Сумма</th><th scope="col" width=100></th></tr>
					</thead>
					<tbody>'.$mech.'</tbody>
					<tfoot>'.$foot.'</tfoot>
				</table>
			</div>';
	}

	function card($domain,$pho,$idorder,$border=true) {	
		
		if ((!$pho)||(!$idorder)) return;
			
		$statuses=array("","Обработка","Неверный номер","Выполнен","Нет ответа","Отменен","Ожидает оплаты");
		
		$statusesC=array(""=>"Новый клиент",0=>"Клиент",1=>"Черный список",2=>"Вип",3=>"Промо",4=>"Сотрудник");
				
		$pho=preg_replace('/[^0-9]/','',$pho);
		$ps=substr($pho,-10,10);
		
		$aolist=SelectDBx("`orders`.*","`orders`,`phones`","`orders`.`idname`=`phones`.`id` AND `phones`.`phone` like '%".$ps."%'",11,"`id` DESC");

		$agi=SelectDBx("DISTINCT `catalog`.`id`,`catalog`.*","`catalog`,`catalogdom`","`catalog`.`type`=1 AND  `catalog`.`id`=`catalogdom`.`item` AND `catalogdom`.`domain`=".$domain,0,"`id` DESC");
		
		if ($agi) {
			$idgift=$agi[0]['id'];
		} else $idgift=0;	
		
		$olist="";
		$isgift=" не ";
		
		if ($aolist) foreach ($aolist as $i => $n) {
		
			$ido=$n['id'];

			$client=SelectDBf("phones",$n['idname'],"name");
			$phone=SelectDBf("phones",$n['idname'],"phone");

			$tso=SelectDBfq("listts","`type`=0 AND `idorder`=$ido","ts");
			list($d,$time)=explode(" ",$tso);
			
			$ll=SelectDBf("locations",$n['location'],"ru");
			list($location,$inoc)=explode("|",$ll);

			$da=json_decode(htmlspecialchars_decode(SelectDBf("listadr",$n["idadr"],"adr")),true);
			$adr=$location.", ";
			
			if ($da['street']) $ada=$da['street'].($da['house']?",".$da['house']:"");
				elseif ($da['address']) $ada=$da['address'];
			$adr.=$ada.", ";
			
			if ($da['apartment']) $adr.="кв.".$da['apartment'].", ";
			if ($da['entrance']) $adr.="под. ".$da['entrance'].", ";
			if ($da['floor']) $adr.="эт. ".$da['floor'];		
			
			$aa[$da['street']."~".$da['house']."~".$da['apartment']."~".$da['entrance']."~".$da['floor']."~".$client."~".$phone]=$adr;

			if ($ido!=$idorder) $olist.="<a target='_blank' href='/admin/editorder?id=$ido'>№".$ido." от ".dateL($d)." ".$statuses[$n['status']]." на сумму ".$n['summ']."</a><br>";
			
			$aoit=selectDB("items","`idorder`=$ido AND `id`=$idgift");
			if ($aoit) {
				$isgift=" ";
				$dg=SelectDBf("orders",$n['id'],"dt");
				$dgift=dateL($dg);
			}			
			
		}

		$alist="";		
		if ($aa) foreach ($aa as $i => $n) {
			if ($border) $alist.=$n.'<br>';
				else $alist.='<div class="setadr" mark="'.$i.'"><i class="fa fa-copy"></i> '.$n.'</div>';
		}	

		if ($agi) {
			$glist=$agi[0]['ru'].$isgift."получен ".$dgift;
		}

		$os=SelectDBx("`orders`.*","`orders`,`phones`","`orders`.`idname`=`phones`.`id` AND `phones`.`phone` like '%".$ps."%'",0,"`id` DESC");
		$qty=0;
		foreach ($os as $i => $n) {
			$qty=$qty+1;
		}			

		$card=SelectDBv("cards","`id` like '%".$ps."%' AND `domain`=".$domain);
		if ($card) {
			UpdateDB("cards","`id` like '%$ps%' AND `domain`=".$domain,array("orders"=>$qty));
			if ($card['cause']==1) $promo="<h6 style='color:red'>ПРОМО</h6>";
		}	
		else {
			InsertDB("cards",array("id"=>$ps,"name"=>$name,"domain"=>$domain,"orders"=>1));
			$card=SelectDBv("cards","`id` like '%".$ps."%' AND `domain`=".$domain);
		}		
		
		if ($card['allergy']!="") $allergy='<h6>Аллергия:</h6><span style="color:#F00">'.$card['allergy'].'</span><br>';
		if ($card['mark']!="") $mark='<h6>Примечание:</h6><span style="color:#F00">'.$card['mark'].'</span><br>';

		$ml=SelectDB("notes","`phone` LIKE '%".$ps."%'");
		if ($ml) {
			$marklist="<h6>История:</h6>";
			foreach($ml as $i =>$z)
				$marklist.="<sub>".$z['ts']."</sub> ".$z['note']."<br>";
		}

		$return='<a target="_blank" href="/admin/card?id='.$ps.'">'.$promo.'<h6><i class="fa fa-id-card-o"></i> '.$name.' ('.$statusesC[$card['status']].')'.($qty?" Заказов : ".$qty:"").'</h6></a><h6>Последние заказы:</h6>'.$olist.$allergy.'<h6>Адреса:</h6>'.$alist.$mark.$marklist.'<h6>Подарки:</h6>'.$glist;
		
		if ($border) return '<div class="bgc-white bd bdrs-3 p-10">'.$return.'</div>'; else return $return;
		
	}
	
	function SmartUnS($s) {
		$data=unserialize($s);
		if (count($data)<2) {
			$j=strtr($s,array('""'=>'" "'));
			if (preg_match_all('/"([^"]+)"/', $j, $m)) {
				foreach ($m[0] as $i => $n) {
					if ($i%2) continue;
					$v=substr($m[0][$i+1],1,-1);
					$ix=substr($n,1,-1);
					if ($v!=" ") $data[$ix]=$v; else $data[$ix]="";
				}
			}
		}
		return $data;
	}
	
	function GenOrderTx($idorder,$domain=1) {
		
		$tpm=SelectDBc("tpm");
		$tsp=SelectDBc("tsp");		
		$statuses=SelectDBc("tstatuses");

		$o=SelectDBv("orders","`id`=$idorder");
		
		$domain=$o['domain'];
		$curr=SelectDBf("currencies",SelectDBf("domains",$domain,"currency"),"ru");
		
		$client=SelectDBf("phones",$o['idname'],"name");
		$phone=SelectDBf("phones",$o['idname'],"phone");

		$crd=SelectDBv("cards","`id` like '%$phone%'");
		$allergy=$crd["allergy"];
		$mark=$crd["mark"];

		$ll=SelectDBf("locations",$o['location'],"ru");
		list($location,$inoc)=explode("|",$ll);

		$da=json_decode(htmlspecialchars_decode(SelectDBf("listadr",$o["idadr"],"adr")),true);
		$adr=$location.", ";
		
		if ($da['street']) $ada=$da['street'].($da['house']?",".$da['house']:"");
			elseif ($da['address']) $ada=$da['address'];
		$adr.=$ada.", ";
		
		if ($da['apartment']) $adr.="кв.".$da['apartment'].", ";
		if ($da['entrance']) $adr.="подъезд ".$da['entrance'].", ";
		if ($da['floor']) $adr.="этаж ".$da['floor'];
		
		list($sum,$qty)=SummQtyOrder($idorder);
				
		$tso=SelectDBfq("listts","`type`=0 AND `idorder`=$idorder","ts");
		list($dateo,$timeo)=explode(" ",$tso);

		$tsClient=SelectDBfq("listts","`type`=20 AND `idorder`=$idorder","ts");
		list($datec,$timec)=explode(" ",$tsClient);
		$time=$timeCLIENT=strtr($tsClient,array("00:00:00"=>"Ближайшее"));

		$tsto=SelectDBfq("listts","`type`=22 AND `idorder`=$idorder","ts");
		list($dateto,$timeto)=explode(" ",$tsto);

		$tsfrom=SelectDBfq("listts","`type`=23 AND `idorder`=$idorder","ts");
		list($datefrom,$timefrom)=explode(" ",$tsfrom);

		$totime="";
		if ($timeto) $totime=" с ".$timeto;
		if ($timefrom) $totime.=" по ".$timefrom;
		if (!$timeto) $totime=strtr(" $timeCRM",array("00:00:00"=>"Ближайшее"));		

		$tsCRM=SelectDBfq("listts","`type`=21 AND `idorder`=$idorder","ts");
		if ($tsCRM) {
			list($dateCRM,$timeCRM)=explode(" ",$tsCRM);
			$timeCRM=strtr($timeCRM,array("00:00:00"=>"Ближайшее"));
			$time=$dateCRM." ".$timeCRM;
		}
		
		$time.=$timeto;		

		$pay=$tpm[$o['pm']];
		if (($o['pm']==1)&&($o['lurl']=="")) $pay=$tpm[3];
		
		$ttt="";
		
		$statusp=" (".$tsp[$o['sp']].")";
		
		if (($o['lurl']!="")&&($o['pm']>0)&&($o['pm']<4))
		switch($o['pstatus']) {
			case "error" : $statusp.="<br>Платеж не найден"; break;
			case "failure" : $statusp.="<br>Неуспешный платеж"; break;
			case "success" : $statusp.="<br>Успешный платеж"; break;
			case "reversed" : $statusp.="<br>Платеж возвращен"; break;
			case "wait_secure" : case "3ds_verify" : $statusp.="<br>Платеж на проверке"; break;
			case "try_again" : $statusp.="<br>Оплата неуспешна. Клиент может повторить попытку еще раз"; break;		
		}

		$dp=json_decode(htmlspecialchars_decode(SelectDBfq("comments","`type`=0 AND `id`=$idorder","comment")),true);
		$persons=$dp['person'];
		if ($persons=="") $persons=1;

		if (strlen($persons)<3) $persons.=" человек(а)";

		$tx="#".$idorder."\n\n";
		$tx.="Время доставки : ".$time."\n\n";
		$tx.="Форма оплаты : ".$pay.$statusp."\n\n";

		$tx.=$client." ".$phone."\n";
		$tx.=$adr."\n\n";
		
		if ($dp['nocallback']) $tx.="Не перезванивать\n\n"; else $tx.="\n";
		
		if ($dp['change']>$sum) $tx.="Сдача с ".$dp['change']." ".$curr."\n\n";
		$tx.=$persons."\n";
		
		if ($dp['noring']) $tx.="Не звонить в дверь\n";
		if ($dp['underdoor']) $addon.="Оставить под дверью\n";		
		if ($dp['cat']) $tx.="Кот \n";
		if ($dp['dog']) $tx.="Собака \n";

		$comm=htmlspecialchars_decode(SelectDBfq("comments","`type`=1 AND `id`=$idorder","comment"));
		if ($comm) $tx.=$comm."\n";

		$commc=htmlspecialchars_decode(SelectDBfq("comments","`type`=2 AND `id`=$idorder","comment"));
		if ($commc) $tx.="\nКомментарий для кухни : ".$commc."\n";

		if ($allergy) $tx.="\nАллергия : ".$allergy."\n";
		if ($mark) $tx.="\nОсобенности клиента : ".$mark."\n";
		
		$tx.="\n";
		
		$sumb=0;
		$at=selectDB("items","`idorder`=$idorder",0,"`ts` ASC");		
		foreach ($at as $i0 => $tr) {
			$name=selectDBf("catalog",$tr['id'],"ru");
			$type=selectDBf("catalog",$tr['id'],"type");			
			$atx[$type].=$name." ".$tr['qty']."\n";
			$sumb+=$tr["bonus"];
		}
		$tx.=$atx[1].$atx[2].$atx[0].$atx[3];
		
		if ($sumb) $tx.="\nСкидка ".$sumb." ".$curr;
		$tx.="\nВсего ".$sum." ".$curr."\n\n";
		
		return $tx;
	}
	
	function SummQtyOrder($id) {
		$at=selectDB("items","`idorder`=$id",0,"`ts` ASC");
		$sum=0;
		$qtys=0;
		if ($at) foreach($at as $j=>$it) {
			$qtys+=$it['qty'];
			$sum+=($it['qty']*$it['price'])-$it['bonus'];
		}
		return array($sum,$qtys);
	}

	function PayStatusOrder($id) { /// 0 - 1 -2
		$status=0;
		$o=selectDBv("orders","`id`=$id");
		if ($o) { 
			$paystatus=$o["typepay"];
			switch ($o["pm"]) {
				case 0 : if ($paystatus==1) $status=2; break;
				case 1 : case 4 : $status=0; break;
				case 2 : $status=2; break;
				case 3 : $status=1; break;
				case 5 : if ($paystatus==3) $status=2; break;
				case 6 : if ($paystatus==4) $status=2; break;
			}
		}
		return $status;
	}

	function statuspay($id) {
		
		$ru=array(		
"success" => "Успешный платеж",
"payment_not_found" => "Платеж не найден",
"error" => "Платеж не создан",
"failure" => "Сбой оплаты",
"reversed" => "Платеж возвращен",
"wait_secure" => "Платеж на проверке",
"3ds_verify" => "Требуется 3DS верификация",
"try_again" => "Оплата неуспешна. Клиент может повторить попытку еще раз",
"expired_3ds" => "Истекло время 3DS верификации",
"senderapp_verify" => "Ожидается подтверждение в приложении SENDER",
"wait_sender" => "Ожидается подтверждение в приложении Privat24/SENDER",
"p24_verify" => "Ожидается завершение платежа в Приват24",
"mp_verify" => "Ожидается завершение платежа в кошельке MasterPass",
"Your payment session has timed out" => "Время вашего платежного сеанса истекло",
"Withdrawal limit already reached" => "Лимит снятия с карты уже достигнут",
"Failed to make payment. Please make sure the parameters are entered correctly and try again" => "Не удалось произвести оплату. Пожалуйста, убедитесь, что параметры введены правильно и попробуйте снова",
"Insufficient funds" => "Недостаточно средств на карте",
"3DSecure unavailable" => "Сервис 3DSecure недоступен",
"Limit is exceeded" => "Превышен лимит карты",
"Authentication failure" => "Ошибка аутентификации",
"System error" => "Системная ошибка банка",

		);
		
		$codes=array(
'0' => "Заказ зарегистрирован, но не оплачен",
'1' => "Cумма забронирована",
'2' => "Успешный платеж",
'3' => "Авторизация отменена",
'4' => "По транзакции была проведена операция возврата",
'5' => "Инициирована авторизация через ACS банка-эмитента",
'6' => "Авторизация отклонена"
		);

		$errors=array(
'0' => "Обработка запроса прошла без системных ошибок",
'1' => "Ожидается orderId или orderNumber",
'5' => "Доступ запрещён",
'7' => "Системная ошибка.",
);

		include($_SERVER['DOCUMENT_ROOT'].'/api/LiqPay.php');
			
		$o=SelectDB("orders","`id`=$id");
		if ($o) {
			$domain=$o[0]['domain'];
			$paymethod=$o[0]['paymethod'];
			$location=$o[0]['location'];
			$orderid=$o[0]['orderid'];
			$l=SelectDB("locations","`id`=".$location);
			if ($l) {
				$public_key=$l[0]['public'];
				$private_key= $l[0]['private'];
			}
		}		
		
		$out=array();

		switch ($public_key) {
			case "i15968698791" :
				if ($id<=28169) {
					$public_key = "i71040424748";
					$private_key = "RrlbYiVovswGUsT2znAKkX8rjEnfpnYaEuqcEiQT";
				}
			break;
		}

		switch ($domain) {
		
			case 1 :
			
				$params = array(
					"action"=>"status",
					"version"=>"3",
					"public_key"=>$public_key,
					"order_id"=>$id
				);						
				$liqpay = new LiqPay($public_key, $private_key);
								
				$res = $liqpay->api("request",$params);
								
				$out['номер транзакции']=$res->payment_id;
				
				$ispay=$res->status=="success";
				$out['оплачен']=($ispay?"оплачен":"не оплачен");
				
				switch ($res->paytype) {
					case "" : $pm=SelectDBf("orders",$id,"pm"); break;
					case "apple_pay" : case "apay" : $pm=1; break; 
					default : $pm=2; break; 
				}	
				
				if (($pm==1)||($pm==2)) UpdateDB("orders","`id`=$id",array("pm"=>$pm, "sp"=>($ispay?2:3),"ps"=>$res->status));

				$out['статус']=$res->status;
				$out['статус*']=$ru[$res->status];
				$out['тип']=$res->paytype;
				$out['сумма']=$res->amount;
				
				$out['ошибка']=$res->err_code;
				$out['ошибка*']=$ru[$res->err_code];
				
				$out['описание ошибки']=$res->err_description;
				$out['описание ошибки*']=$ru[$res->err_description];
				
				$out['время']=Date("Y-m-d H:i:s",$res->create_date/1000);

				if ($out['статус']=="success") {
					$t=SelectDBfq("listts","`idorder`=$id AND `type`=25","ts");
					if (!$t) {						
						InsertDB("listts",array("idorder"=>$id,"type"=>25,"ts"=>$out["время"]));
					}
				} else DeleteDB("listts","`idorder`=$id AND `type`=25");

			break;
			
			case 3 :
				if ($orderid) $req='https://securepayments.sberbank.ru/payment/rest/getOrderStatusExtended.do?userName='.$public_key.'&password='.$private_key.'&orderId='.$orderid; 
					else $req='https://securepayments.sberbank.ru/payment/rest/getOrderStatusExtended.do?userName='.$public_key.'&password='.$private_key.'&orderNumber='.$id;
				$j=file_get_contents($req);
				if ($j!="") $res=json_decode($j);
								
				$out['номер транзакции']=$res->attributes[0]->value;

				$ispay=$res->orderStatus==2;
				$out['оплачен']=($ispay?"оплачен":"не оплачен");
				$status=($ispay?"success":"failure");
				
				UpdateDB("orders","`id`=$id",array("sp"=>($ispay?2:3),"ps"=>$status));

				$out['статус']=$codes[$res->orderStatus];
				$out['тип']=$res->paymentAmountInfo->paymentState;
				$out['сумма']=$res->paymentAmountInfo->approvedAmount/100;
				
				$out['ошибка']=$errors[$res->errorCode];				
				$out['описание ошибки']=$res->errorMessage;
				
				$out['время']=Date("Y-m-d H:i:s",$res->date/1000);

			break;
		}
		return $out;
	}
	
	function deadline($id) {
		$o=selectDBv("orders","`id`=$id");
		if ($o['statusdel']>1) {
			$tz=$o['time0'];
			if (empty($tz)) $tz=$o['time1'];
			if (empty($tz)) $tz=$o['time2'];
			if (empty($tz)) $tz=$o['time3'];
			if (empty($tz)) $tz=$o['time-1'];
			$t0=(timetosec($tz)+$o['minutes']*60)%(24*60*60);
			if ($o['timefrom']) $t0=timetosec($o['timefrom']);
			$t1=$t0;
			if ($o['timeto']) {
				$t1=timetosec($o['timeto']);
				if (empty($o['timefrom'])) $t0=$t1;
			}
			$t=(int)$t0+(($t1-$t0)/2);
			return sectotime($t);
		} else return "";
	}

	function TokenConst($id) {
		return md5("ZSd3".$id."wD2A");
	}

	function FullAddrPoint($id) {	
		$o=SelectDBv("orders","`id`=$id");
		$dat=SmartUnS($o['data']);
		if ($dat['adr2']) $adr.=$dat['adr2'];
		if ($dat['adr4']) $adr.="/ ".$dat['adr4'];
		if ($dat['adr1']) $adr.=", ".$dat['adr1'];
		$adr.=", ".SelectDBf("locations",$o['location'],"name");
		return $adr;
	}

	function FullAddrPointN($id) {
		$o=SelectDBv("orders","`id`=$id");
		$dat=SmartUnS($o['data']);
		if ($dat['adr2']) $adr.=$dat['adr2'];
		if ($dat['adr4']) $adr.="/ ".$dat['adr4'];
		if ($dat['adr1']) $adr.=", ".$dat['adr1'];
		$adr.=", ".SelectDBf("locations",$o['location'],"name");
		return $adr;
	}

	function coor($adr) {
		
		file_put_contents($_SERVER['DOCUMENT_ROOT']."/api/coolog.asp",$adr."\n",FILE_APPEND);
		
		$locations=array("Kyiv"=>2);
		
		$coo=SelectDB("coors","`adr` like '$adr'");
		if ($coo) return array("location"=>$coo[0]['location'],"lat"=>$coo[0]['lat'],"lng"=>$coo[0]['lng']);
		else {
			$url = "http://www.mapquestapi.com/geocoding/v1/address?key=YWs3T5DrphMluiGzC35quU6XHpI81aCL&location=".urlencode($adr);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_HEADER, FALSE);
			$o=curl_exec($ch);
			
			file_put_contents($_SERVER['DOCUMENT_ROOT']."/api/coolog.asp",$o."\n\n",FILE_APPEND);
			
			$j=json_decode($o,true);
			$locs=$j['results'][0]['locations'];
			if (!empty($locs)) foreach ($locs as $i=>$l) {
				$location=$locations[$l['adminArea5']];
				if ($location) {
					InsertDB("coors",array("location"=>$location,"adr"=>$adr,"lat"=>$l['latLng']['lat'],"lng"=>$l['latLng']['lng']));
					return array("location"=>$location,"lat"=>$l['latLng']['lat'],"lng"=>$l['latLng']['lng']);
				} else {
					$location=2;
					list($lat,$long)=explode(",",SelectDBf("locations",$location,"center"));
					return array("location"=>$location,"lat"=>$lat,"lng"=>$long);
				}	
			}
			return array();
		}
	}

	function coorVC($idorder) {


		$o=SelectDBv("orders","`id`=$idorder");
		$location=$o['location'];
		$dat=unserialize($o['data']);
		$adr=SelectDBf("locations",$location,"name").", ";
		if ($dat['adr1']) $adr.=$dat['adr1'].", ";
		if ($dat['adr2']) $adr.="д. ".$dat['adr2'];

		file_put_contents($_SERVER['DOCUMENT_ROOT']."/api/coolog.asp",$adr."\n",FILE_APPEND);

		$coo=SelectDB("coors","`adr` like '$adr'");
		$coo=0;
		if ($coo) return array("lat"=>$coo[0]['lat'],"lng"=>$coo[0]['lng']);
		else {
					
			$url = "https://api.visicom.ua/data-api/4.0/ru/geocode.json?text=".urlencode($adr)."&key=8e8f9e956d291b4b10e0cf9a540bdfa4";
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_HEADER, FALSE);
			$o=curl_exec($ch);
			$j=json_decode($o);
			
			if ($j->type=="FeatureCollection") {
				$street=$j->features[0]->properties->street;
				$name=$j->features[0]->properties->name;
				$lng=$j->features[0]->geo_centroid->coordinates[0];
				$lat=$j->features[0]->geo_centroid->coordinates[1];
			}
			else {
				$street=$j->properties->street;
				$name=$j->properties->name;
				$lng=$j->geo_centroid->coordinates[0];
				$lat=$j->geo_centroid->coordinates[1];
			}

			file_put_contents($_SERVER['DOCUMENT_ROOT']."/api/coolog.asp",$lat." ".$lng,"\n\n",FILE_APPEND);

			InsertDB("coors",array("location"=>$location,"adr"=>$adr,"lat"=>$lat,"lng"=>$lng));
			return array("location"=>$location,"lat"=>$lat,"lng"=>$lng);
						
		}
	}

	function SmartButton($type,$id,$status,$simple) {
	
		$statuses=SelectDBc("typessd");
		
		$masks=array(
			0 => array (
				-1 => array(0,6),
				0 => array(-1,1,6),
				1 => array(2,6),
				2 => array(0,1,3,6),
				3 => array(2,4,5,6),
				4 => array(3,5,6),
				5 => array(3,4,6),
				6 => array(3,4,5),
				7 => array()
			),
			1 => array (
				-1 => array(0,1,2,3,6),
				0 => array(-1,1,2,3,6),
				1 => array(0,2,3,6),
				2 => array(1,3,6),
				3 => array(2,4,5,6),
				4 => array(3,5,6),
				5 => array(3,4,6),
				6 => array(3,4,5),
				7 => array()
			),
			2 => array (
				-1 => array(),
				0 => array(),
				1 => array(),
				2 => array(0,1,3),
				3 => array(2,4),
				4 => array(),
				5 => array(),
				6 => array(),
				7 => array()
			),
			
		);
		
		$mask=$masks[(int)$simple][(int)$status];
		
		if (empty($mask)) {
			$res='<button type="button" class="btn btn-primary">'.$statuses[$status].'</button>';
		}	
		else {

			$list="";
			$next=-1;
			foreach($mask as $i=>$n) {
				$list.='<span class="dropdown-item setstatusai" op="'.$type.'" ido="'.$id.'" status="'.$n.'">'.$statuses[$n].'</span>';
				if (($next<0)&&($n>$status)) $next=$n;
			}
			if ($next<0) $next=$status;

			$res='
			<div class="btn-group" id="'.$type.$id.'">
				<button type="button" class="btn btn-primary setstatusai" op="'.$type.'" ido="'.$id.'" status="'.$next.'">'.$statuses[$status].'</button>
				<button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="padding:2px 10px"><span class="sr-only">v</span></button>
				<div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(64px, 35px, 0px); top: 0px; left: 0px; will-change: transform;">'.$list.'
				</div>
			</div>';

		}
		return $res;
	}

	function Distance ($latA, $lngA, $latB, $lngB) {
		define('EARTH_RADIUS', 6372795);
		$lat1 = $latA * M_PI / 180;
		$lat2 = $latB * M_PI / 180;
		$long1 = $lngA * M_PI / 180;
		$long2 = $lngB * M_PI / 180;	 
		$cl1 = cos($lat1);
		$cl2 = cos($lat2);
		$sl1 = sin($lat1);
		$sl2 = sin($lat2);
		$delta = $long2 - $long1;
		$cdelta = cos($delta);
		$sdelta = sin($delta);	 
		$y = sqrt(pow($cl2 * $sdelta, 2) + pow($cl1 * $sl2 - $sl1 * $cl2 * $cdelta, 2));
		$x = $sl1 * $sl2 + $cl1 * $cl2 * $cdelta;
		$ad = atan2($y, $x);
		$dist = $ad * EARTH_RADIUS;
		return $dist;
	}

	function listAddress($city,$adr) {
		
		$url = "https://api.visicom.ua/data-api/4.0/ru/geocode.json?text=".urlencode($city.",".$adr)."&key=8e8f9e956d291b4b10e0cf9a540bdfa4";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		$o=curl_exec($ch);
		$j=json_decode($o);
		
		if ($j->type=="FeatureCollection") {
		
			foreach ($j->features as $i=>$fc) {							
				$p=array("lat"=>$fc->geo_centroid->coordinates[0],"lng"=>$fc->geo_centroid->coordinates[1]);
				$type[]=$fc->properties->type;
				$street[]=$fc->properties->name;
				$address[]=$fc->properties->address;
				$cat[]=$fc->properties->categories;
				$settlement[]=$fc->properties->settlement_id;
				$zone[]=$fc->properties->zone;
				$point[]=$p;
				$aeq[$fc->properties->name]++;
			}
		}
		else {
			$p=array("lat"=>$j->geo_centroid->coordinates[0],"lng"=>$j->geo_centroid->coordinates[1]);
			$type[]=$j->properties->type;
			$street[]=$j->properties->name;
			$address[]=$j->properties->address;
			$cat[]=$j->properties->categories;
			$settlement[]=$j->properties->settlement_id;
			$zone[]=$j->properties->zone;
			$point[]=$p;
		}					
							
		$asend=array();						
		foreach ($street as $i=>$t) {
			$ad=($type[$i]?$type[$i]." ":"").$street[$i].($zone[$i]&&$aeq[$street[$i]]>1?" (".$zone[$i].")":"");
			if (($cat[$i]!="adr_address")&&($cat[$i]!="adm_district")&&($cat[$i]!="adm_settlement")&&($cat[$i]!="adm_level1")&&($cat[$i]!="poi_airport")&&(($settlement[$i]=="STL1NQ7EP")||(substr($address[$i],0,8)==$city))) {
				if (!array_key_exists($ad,$asend))
					$data[]=array("street"=>($t?"$t ":"").$name[$i],"point"=>$point[$i]);				
				$asend[$ad]=true;
			}
		}
		
		return $data;
	}	

	function send_smsm($phone,$message) {
		$phone=strtr($phone,array("+38"=>""));
		$message=urlencode($message);
		$login="mailsnesmith";
		$password="cv3c@h9n_Ei5S_U";
		return file_get_contents("https://smsc.ru/sys/send.php?login=$login&psw=$password&phones=$phone&mes=$message&sender=Ninja Group");
	}

	//// checkbox

	function curl($url,$j,$token,$key) {
		$h=array('Content-Type:application/json','Authorization: Bearer '.$token);
		if ($key) $h[]='X-License-Key: '.$key;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://api.checkbox.in.ua".$url);
		curl_setopt($ch, CURLOPT_POST, 1);	
		curl_setopt($ch, CURLOPT_HTTPHEADER, $h );
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$j);		
		$res=json_decode(curl_exec($ch));
		curl_close($ch);
		return $res;
	}

	function getcurl($url,$token,$key) {
		$h=array('Content-Type:application/json','Authorization: Bearer '.$token);
		if ($key) $h[]='X-License-Key: '.$key;
		$ch = curl_init();		
		curl_setopt($ch, CURLOPT_URL, "https://api.checkbox.in.ua".$url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $h);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$j=curl_exec($ch);
		$res=json_decode($j);
		curl_close($ch);
		return $res;
	}

	function guidv4($data) {
		assert(strlen($data) == 16);
		$data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
		$data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10
		return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
	}
	
	function getToken() {
		$j = curl("/api/v1/cashier/signin",'{ "login": "obolonwa2", "password": "qaz320" }',0,0);
		return $j->access_token;
	}

	function getReceipt($id) {
		GLOBAL $access_token;
		$j=getcurl("/api/v1/receipts/".$id,$access_token,0);
		switch ($j->transaction->status) {
			case "DONE" : UpdateDB("orders","`uid`='".$j->id."'",array("fixstatus"=>5,"fixno"=>$j->fiscal_code)); break;
			case "PENDING" : UpdateDB("orders","`uid`='".$j->id."'",array("fixstatus"=>2)); break;
			case "SIGNED" : UpdateDB("orders","`uid`='".$j->id."'",array("fixstatus"=>3)); break;
			case "DELIVERED" : UpdateDB("orders","`uid`='".$j->id."'",array("fixstatus"=>4)); break;
			case "ERROR" : UpdateDB("orders","`uid`='".$j->id."'",array("fixstatus"=>-1)); break;
		}
		return $j;
	}

	function openDay() {
		GLOBAL $access_token;
		$j = curl("/api/v1/shifts",json_encode($a),$access_token,"6680373eaa424584205bb4e1");
		return $j;
	}

	function closeDay() {
		GLOBAL $access_token;
		$j = curl("/api/v1/shifts/close",json_encode($a),$access_token,0);
		return $j;
	}

	function statusPair() {
		GLOBAL $access_token,$zreport;
		$j=getcurl("/api/v1/shifts?desc=true&limit=1",$access_token,0);
		$zreport=$j->results[0];
		return $j->results[0]->status;
	}
	
	function sendReceipt($idorder) {
	
		GLOBAL $access_token;
		
		$today=date("Y-m-d");
	
		$a=array();
		
		$uid=guidv4(openssl_random_pseudo_bytes(16));
		
		$a['id']=$uid;
		
		$a['cashier_name']="Глазов Д.";
		$a['departament']="WOK";
		
		$o=SelectDBv("orders","`id`=$idorder");
		
		if ($o['uid']) return json_decode('{ "error": 409 , "message" : "Receipt has already been created" }');
		
		if ($o['sp']==2) {
					
			$it=SelectDB("items","`idorder`=$idorder",0,"`ts` ASC");
			
			$a['goods']=array();
			
			$total=0;
			
			if ($o['pm']) $type="CASHLESS"; else $type="CASH";
			
			foreach ($it as $i =>$n ) {
				
				$g=SelectDBv("catalog","`id`=$n[id]");
				$price=SelectDBfq("prices","`domain`=1 AND `item`=$n[id]","price");
						
				if ($n['bonus']>0) {
					$discounts=array(array("type"=>"DISCOUNT","mode"=>"VALUE","value"=>$n['bonus']*100));
				}
				else $discounts=array();
						
				$a['goods'][] = array (
				  "good" => array(
					"code" =>  $g["code"],
					"name" => $g["ua"],
					"barcode" => "",
					"header"  => "",
					"footer"  => "",
					"price" => $price*100,
					"tax" => array(),
					"uktzed" => ""
				  ),
				  "quantity"=>$n['qty']*1000,
				  "is_return"=>false,
				  "discounts"=>$discounts
				);
				
				$total+=$n['qty']*$price*100;
				
			}
			
			$a['discounts']=array();
			$a['delivery']=array("email"=>"checkninjasusi@gmail.com");
			$a['payments']=array(array("type"=>$type,"value"=>$total));

			$j = curl("/api/v1/receipts/sell",json_encode($a),$access_token,0);

		}
				
		if ($j->id==$uid) {
			$tsum=$j->total_sum;
			$tpay=$j->total_payment;
			UpdateDB("orders","`id`=$idorder",array("uid"=>$uid,"uid"=>$uid,"tsum"=>$tsum,"tpay"=>$tpay,"fixstatus"=>1,"fxdt"=>$today));
		}
		
		return $j;

	}	

?>