<?php
if (!session_start()) {
    session_start();
}
ob_start();


function GetOfflineCodes() {

    require_once $_SERVER['DOCUMENT_ROOT']."/conf/pdo.php";

    $url = 'https://api.checkbox.in.ua/api/v1/cash-registers/get-offline-codes';

    $headers = [
        "Content-Type:application/json",
        "X-License-Key: test90c363caa3cee3b0885a4747",
        "Authorization: Bearer ".$_POST['token']
    ];

    $json="";
    $curl = curl_init();

    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_VERBOSE, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_POST, false);


    $result = curl_exec($curl);

    $array = json_decode($result);

    foreach ($array as $stdClass){


        $pdo2 = $pdo->prepare("INSERT INTO offline_codes (cash_register_id, created_at, serial_id, fiscal_code) VALUES (?,?,?,?)");
        $pdo2->bindParam(1, $stdClass->cash_register_id);
        $pdo2->bindParam(2,$stdClass->created_at);
        $pdo2->bindParam(3,$stdClass->serial_id);
        $pdo2->bindParam(4,$stdClass->fiscal_code);
        $pdo2->execute();
    }


}
GetOfflineCodes();




//cash_register_id: "1cb4f241-1258-44bc-bb01-ac426b7d4836"
//created_at: "2022-01-16T08:15:16.290804"
//fiscal_code: "TEST-02M5_f"
//serial_id: 1



