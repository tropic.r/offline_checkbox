<?php
if (!session_start()) {
    session_start();
}
ob_start();

require_once __DIR__ . "/curl.php";

function openDay()
{

    $url = 'https://api.checkbox.in.ua/api/v1/shifts';

    $headers = [
        "Content-Type:application/json",
        "X-License-Key: test90c363caa3cee3b0885a4747",
        "Authorization: Bearer " . $_POST['token']
    ];

    $r = [
        "id" => "497f6eca-6276-4993-bfeb-53cbbbba6f08",
    ];

    $json = json_encode($r);

    $curl = curl_init();

    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_VERBOSE, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_POST, true);


    echo $result = curl_exec($curl);
}
openDay();