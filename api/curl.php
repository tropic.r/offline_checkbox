<?php


function curl($url,$j=false,$token,$key,$post=true) {
    $h=array('Content-Type:application/json','Authorization: Bearer '.$token);
    if ($key) $h[]='X-License-Key: '.$key;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://api.checkbox.in.ua".$url);
    curl_setopt($ch, CURLOPT_POST, $post);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $h );
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,$j);
    $res=curl_exec($ch);
    curl_close($ch);
    return $res;
}
