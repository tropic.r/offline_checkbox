<?php
if (!session_start()) {
    session_start();
}
ob_start();


require_once __DIR__ . "/curl.php";

function GoOfflineAction()
{
    $a = [
        "go_offline_date" => "2019-08-24T14:15:22Z",
        "fiscal_code" => "string"
    ];

    $j = curl("/api/v1/cash-registers/go-offline", json_encode($a), $_SESSION['_token'], "6680373eaa424584205bb4e1");
}

GoOfflineAction();
