import ajax from "./ajax.js";

function ping() {
    let b=document.querySelector('#db6789w34523')

    if(b){
        b.addEventListener('click',()=>{
            ajax('/api/ping.php',{token:localStorage.getItem('token')}).then(resp=>{
                let obj = JSON.parse(resp)
                let st = document.querySelector('.status')

                st.innerHTML = `<p style="color: #FFF">${obj.status}</p>`

            })
        })
    }
}
export default ping