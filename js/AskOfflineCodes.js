import ajax from "./ajax.js";


function AskOfflineCodes() {
    return new Promise((resolve, reject)=>{
        let b = document.querySelector('#a6789q34527')

        b.addEventListener('click',()=>{
            ajax('/api/AskOfflineCodes.php',{token:localStorage.getItem('token')}).then(response=>{
                console.log(response)
                let obj =  JSON.parse(response)

                if(obj.status=='ok'){
                    resolve()
                }else{
                    reject()
                }
            })
        })
    })

}
export default AskOfflineCodes