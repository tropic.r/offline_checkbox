let ajax = (url,data={})=>{
    return new Promise((resolve, reject)=>{
        let xhr = new XMLHttpRequest();
        let formData = new FormData();

        if(Object.keys(data).length>0){
            for(let key in data){
                formData.append(key,data[key])
            }
        }

        xhr.open("POST",url)

        xhr.onload = ()=>{
           // console.log(xhr.response)

            resolve(xhr.response)
        }
        xhr.onerror = ()=>{
            console.error('ajax error')

            reject();
        }

        xhr.send(formData)

    })

}
export default ajax;